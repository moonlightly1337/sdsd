<?php

use \Phpmig\Adapter;
use Pimple\Container;
use App\Models\Database;
use Illuminate\Database\Capsule\Manager as Capsule;


$container = new Container();

$database = require 'config/autoload/database.global.php';
$db = new Database($database);
$capsule = $db->getCaps();

$container['db'] = function($container) use ($capsule) {
    return $capsule;
};


// replace this with a better Phpmig\Adapter\AdapterInterface
$container['phpmig.adapter'] = new Adapter\File\Flat(__DIR__ . DIRECTORY_SEPARATOR . 'migrations/.migrations.log');

$container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'migrations';

// You can also provide an array of migration files
// $container['phpmig.migrations'] = array_merge(
//     glob('migrations_1/*.php'),
//     glob('migrations_2/*.php')
// );

return $container;
