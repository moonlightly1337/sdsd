<?php

use Phpmig\Migration\Migration;
use App\Models\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager;

class CreateUsers extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        Manager::schema()->create('users_table', function(Blueprint $table) {
            $table->id();
            $table->text('clientId');
            $table->text('accessToken');
            $table->text('refreshToken');
            $table->string('baseDomain');
            $table->integer('expires');
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Manager::schema()->dropIfExists('users_table');
    }
}
