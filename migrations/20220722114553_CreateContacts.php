<?php

use Phpmig\Migration\Migration;
use App\Models\Contact;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager;

class CreateContacts extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        Manager::schema()->create('contacts', function(Blueprint $table) {
            $table->id();
            $table->integer('account_id');
            $table->integer('contact_id');
            $table->text('contact_name');
            $table->text('email')->nullable();
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        Manager::schema()->dropIfExists('contacts');
    }
}
