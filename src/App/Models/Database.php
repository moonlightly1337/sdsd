<?php

namespace App\Models;

use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;

class Database 
{
    private Capsule $capsule;

    public function __construct(array $config) {
        $capsule = new Capsule;
        $capsule->addConnection($config);
        $capsule->setEventDispatcher(new Dispatcher(new Container));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
        $this->capsule = $capsule;
    }

    public function getCaps(): Capsule 
    {
        return $this->capsule;
    }

}


?>
