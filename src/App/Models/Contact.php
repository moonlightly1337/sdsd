<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $primaryKey = 'id';
    public $fillable = [
        'account_id',
        'contact_id',
        'email',
        'account_name',
        'updated_at',
        'created_at',
    ];
}
