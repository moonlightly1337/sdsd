<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Model
{
    protected $table = 'users_table';
    protected $primaryKey = 'id';
    public $fillable = [
        'clientId',
        'accessToken',
        'refreshToken',
        'baseDomain',
        'expires',
        'updated_at',
        'created_at',
    ];
}
