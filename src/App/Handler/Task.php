<?php

declare(strict_types=1);

namespace App\Handler;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Models\Contact;


class Task extends Command
{
    private Beanstalk $beanstalk;
    public $ApiKey;

    public function __construct(Beanstalk $beanstalk, $ApiKey) {
        parent::__construct();
        $this->beanstalk = $beanstalk;
        $this->ApiKey = $ApiKey;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $pheanstalk = $this->beanstalk->getClient();
        $output->writeln('worker: started account sync');
        $job = $pheanstalk->watchOnly('account_sync')
                          ->ignore('default')
                          ->reserve();
        $contacts = Contact::get();
        foreach($contacts as $contact) {
            $output->writeln($this->addContact($this->ApiKey, $contact['email']));  
        }
        $pheanstalk->delete($job);
        $output->writeln('worker: account sync over');
        return 0;
    }

    public function addContact($apikey, $email) {
        $link = "https://api.unisender.com/ru/api/importContacts?format=json&api_key=$apikey&field_names[0]=email&data[0][0]=$email";
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}