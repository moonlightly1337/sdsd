<?php

declare(strict_types=1);

namespace App\Handler;

use AmoCRM\Client\AmoCRMApiClient;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ApiFactory implements FactoryInterface
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): RequestHandlerInterface
    {
        $ApiKey = $container->get('config')['apikey'];
        $RedirectURL = $container->get('config')['client_info']['redirectURI'];
        return new ApiHandler($container->get(AmoCRMApiClient::class), 
                              $ApiKey, 
                              $RedirectURL,
                              $container->get(Beanstalk::class)
                            );
    }
}
