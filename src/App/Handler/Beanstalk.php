<?php

declare(strict_types=1);

namespace App\Handler;

use Pheanstalk\Job;
use Pheanstalk\Pheanstalk;

class Beanstalk
{
    private Pheanstalk $queue;
    public string $host;

    public function __construct(string $host)
    {
        $this->queue = Pheanstalk::create('127.0.0.1');
    }

    public function createTask(): Job
    {
        return $this->queue->useTube('account_sync')->put(json_encode('123'));
    }

    public function getClient(): Pheanstalk
    {
        return $this->queue;
    }
}
