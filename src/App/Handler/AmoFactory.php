<?php

declare(strict_types=1);

namespace App\Handler;

use AmoCRM\Client\AmoCRMApiClient;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

class AmoFactory implements FactoryInterface
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): AmoCRMApiClient
    {
        $config = $container->get('config')['client_info'];

        return new AmoCRMApiClient(
            $config['clientId'],
            $config['clientSecret'],
            $config['redirectURI'],
        );
    }
}
