<?php

declare(strict_types=1);

namespace App\Handler;

use Dotenv\Dotenv;
use Laminas\Diactoros\Response\RedirectResponse;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Http\Request;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Models\WebhookModel;
use App\Models\User;
use App\Models\Contact;
use App\Handler\Beanstalk;
use Illuminate\Support\Facades\DB;


use function time;

class ApiHandler implements RequestHandlerInterface
{
    public AmoCRMApiClient $client;
    public Beanstalk $beanstalk;
    public function __construct(AmoCRMApiClient $client, string $ApiKey, $redirecturi, $beanstalk) {
        $this->client = $client;
        $this->ApiKey = $ApiKey;
        $this->redirect = $redirecturi;
        $this->beanstalk = $beanstalk;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {   
        $state = bin2hex(random_bytes(16));
        $this->check($request);
        $response = new JsonResponse(['oaky' => Contact::get()]);
        $this->beanstalk->createTask();
        if(isset($request->getQueryParams()['referer'])) {
            $this->client->setAccountBaseDomain($request->getQueryParams()['referer']);
        }
        
        
        if(isset($request->getQueryParams()['code'])) {
            $accessToken = $this->client->getOAuthClient()->getAccessTokenByCode($request->getQueryParams()['code']); 
        }
        $responses = [];
        if ((isset($accessToken)) and (!$accessToken->hasExpired())) {
            $this->client->setAccessToken($accessToken);
            $this->webhookSubscribe($this->client);
            $clientId = $this->client->account()->getCurrent()->getId();
            $date = date('Ymd H:i:s');
            $data = [
                'clientId' => $clientId,
                'accessToken' => $accessToken->getToken(),
                'refreshToken' => $accessToken->getRefreshToken(),
                'expires' => $accessToken->getExpires(),
                'baseDomain' => $this->client->getAccountBaseDomain(),
                'updated_at' => $date,
                'created_at' => $date,
            ];
            $response = new JsonResponse(['result' => $result]);
        }
            
        return $response ?? new JsonResponse(['ok' => 'pl']);
    }

    public function check(ServerRequestInterface $request) {
        if($request->getParsedBody()) {
            $req = $request->getParsedBody();
            if(isset($req['contacts']['add'])) {
                $emails = $this->newContact($request);
            }
            if(isset($req['contacts']['update'])) {
                $emails = $this->updateContact($request);
            }
            /*if($emails) {
                foreach($emails as $email) {
                    $this->addContact($this->ApiKey, $email);
                }
            }*/
        }
        return null;
    }


    public function updateContact(ServerRequestInterface $request) {
        $req = $request->getParsedBody();
        $contact = $req['contacts']['update'];
        $emailArray = [];
        foreach($contact as $cont) {
            $beforeUpdate = Contact::where('contact_id', '=', $cont['id'])->get();
            $emails = $this->getEmail($cont['custom_fields']);
            if($emails != null) {
                foreach($emails as $email) {
                    $switch = true;
                    foreach($beforeUpdate as $before) {
                        $before->contact_name = $cont['name'];
                        $before->save();
                        if(($before['email'] == $email)) {
                            $switch = false;
                        }
                    }
                    if($switch) {
                            $emailArray[] = $email;
                            $newContact = new Contact();
                            $newContact->setAttribute('contact_id', $cont['id'])
                                       ->setAttribute('account_id', $req['account']['id'])
                                       ->setAttribute('contact_name', $cont['name'])
                                       ->setAttribute('email', $email);
                            $newContact->save();
                    }
                }
                if($emailArray) {
                    return $emailArray;
                }
            }
        }
        return null;
    }

    public function newContact(ServerRequestInterface $request) {
        $req = $request->getParsedBody();
        $contact = $req['contacts']['add'];
            foreach($contact as $cont) {
                $emailArray = $this->getEmail($cont['custom_fields']);    
                if($emailArray !== null) {
                    foreach($emailArray as $email) {
                        $newContact = new Contact();
                        $newContact->setAttribute('contact_id', $cont['id'])
                                   ->setAttribute('account_id', $req['account']['id'])
                                   ->setAttribute('contact_name', $cont['name'])
                                   ->setAttribute('email', $email);
                        $newContact->save();
                    }
                }
            }
        return $emailArray ?? null;
    }
    


    public function getEmails($custom_field) {
        if(isset($custom_field)) {
            $array = [];
            foreach($custom_field as $item) {
                if($item['field_code'] == 'EMAIL') {
                    foreach($item['values'] as $value) {
                        $array[] = $value['value'];
                    }
                }
            }
            return $array;
        }
        return null;
    }

    public function getEmail(array $custom_fields) {
        $switch = false;
        $emailArray = [];
        foreach($custom_fields as $field) {
            if($field['name'] == "Email") {
                $switch = true;
                foreach($field['values'] as $value) {
                    $emailArray[] = $value['value'];
                }
            }
        }
        if(!$switch) {
            return null;
        }
        return $emailArray;
    }

    public function webhookSubscribe(AmoCRMApiClient $client) {
        $webhook = new WebhookModel();
        $webhook->setDestination($this->redirect)
                ->setSettings(['add_contact', 'update_contact', ]);
        $webhook = $client->webhooks()->subscribe($webhook);
    }

    public function saveUser($accessToken) {
        try {
            if(isset($accessToken['accessToken'])
            && isset($accessToken['clientId'])
            && isset($accessToken['refreshToken'])
            && isset($accessToken['expires'])
            && isset($accessToken['baseDomain'])) {
                $user = User::query()->where('clientId', '=', $accessToken['clientId'])->get()->first();
                if(empty($user->clientId)) {
                    $user = (new User())
                        ->setAttribute('clientId', $accessToken['clientId'])
                        ->setAttribute('accessToken', $accessToken['accessToken'])
                        ->setAttribute('refreshToken', $accessToken['refreshToken'])
                        ->setAttribute('expires', $accessToken['expires'])
                        ->setAttribute('baseDomain', $accessToken['baseDomain'])
                        ->setAttribute('updated_at', $accessToken['updated_at']);
                    $user->save();
                }else{
                    $user->setAttribute('clientId', $accessToken['clientId'])
                         ->setAttribute('accessToken', $accessToken['accessToken'])
                         ->setAttribute('refreshToken', $accessToken['refreshToken'])
                         ->setAttribute('expires', $accessToken['expires'])
                         ->setAttribute('baseDomain', $accessToken['baseDomain'])
                         ->setAttribute('updated_at', $accessToken['updated_at']);
                    $user->update();
                }
        }
        }catch(\Throwable $exception) {
            return new JsonResponse(['error' => $exception->getMessage()]);
        }
        return new JsonResponse(['okay' => 'saved']);
    }

    public function addContact($apikey, $email) {
        $link = "https://api.unisender.com/ru/api/importContacts?format=json&api_key=$apikey&field_names[0]=email&data[0][0]=$email";
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

}
