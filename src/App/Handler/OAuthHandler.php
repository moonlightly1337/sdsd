<?php

declare(strict_types=1);

namespace App\Handler;
use Laminas\Diactoros\Response\RedirectResponse;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use AmoCRM\Client\AmoCRMApiClient;

use function time;

class OAuthHandler implements RequestHandlerInterface
{
    public AmoCRMApiClient $client;

    public function __construct(AmoCRMApiClient $client) {
        $this->client = $client;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $state = bin2hex(random_bytes(16));
        $authorizationUrl = $this->client->getOAuthClient()->getAuthorizeUrl([
            'state' => $state,
            'mode' => 'post_message',
        ]);
        $response = new RedirectResponse($authorizationUrl);
        
        return $response;
    }
}
