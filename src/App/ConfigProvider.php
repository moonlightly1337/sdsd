<?php

declare(strict_types=1);

namespace App;
use App\Handler\AmoFactory;
use AmoCRM\Client\AmoCRMApiClient;
use App\Handler\OAuthFactory;
use App\Handler\OAuthHandler;
use App\Handler\ApiHandler;
use App\Handler\ApiFactory;
use App\Handler\Beanstalk;
use App\Handler\BeanstalkFactory;
use App\Handler\Task;
use App\Handler\TaskFactory;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.laminas.dev/laminas-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
            'laminas-cli' => $this->getCliConfig(),
        ];
    }

    public function getCliConfig() : array
    {
        return [
            'commands' => [
                'worker:task' => Task::class,
            ],
        ];
    }



    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'invokables' => [
                Handler\PingHandler::class => Handler\PingHandler::class,
            ],
            'factories'  => [
                ApiHandler::class => ApiFactory::class,
                Beanstalk::class=>BeanstalkFactory::class,
                Task::class=>TaskFactory::class,
                AmoCRMApiClient::class => AmoFactory::class,
                OAuthHandler::class => OAuthFactory::class,
                Handler\HomePageHandler::class => Handler\HomePageHandlerFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'app'    => ['templates/app'],
                'error'  => ['templates/error'],
                'layout' => ['templates/layout'],
            ],
        ];
    }
}
