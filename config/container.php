<?php

declare(strict_types=1);

use Laminas\ServiceManager\ServiceManager;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Events\Dispatcher;



// Load configuration
$config = require __DIR__ . '/config.php';

$dependencies                       = $config['dependencies'];
$dependencies['services']['config'] = $config;

// Build container


$capsule = new Manager();
$capsuleContainer = $capsule->getContainer();
$capsule->addConnection($config['database'], 'main');
//$capsule->setEventDispatcher(new Dispatcher(new Container));
$capsule->getDatabaseManager()->setDefaultConnection('main');
$capsule->setAsGlobal();
$capsule->bootEloquent();


return new ServiceManager($dependencies);
