<?php

declare(strict_types=1);
use Dotenv\Dotenv;
$dotenv = Dotenv::createUnsafeImmutable(dirname(dirname(__DIR__)));
$dotenv->load();
return [
    'client_info' =>[
        'clientId' => getenv('clientId'),
        'clientSecret' => getenv('clientSecret'),
        'redirectURI' => getenv('redirectURI'),
    ],
];
?>
