<?php

declare(strict_types=1);
use Dotenv\Dotenv;
$dotenv = Dotenv::createUnsafeImmutable(dirname(dirname(__DIR__)));
$dotenv->load();
return [
    'apikey' => getenv('ApiKey'),
];
?>
