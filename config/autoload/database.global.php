<?php

declare(strict_types=1);
use Dotenv\Dotenv;
$dotenv = Dotenv::createUnsafeImmutable(dirname(dirname(__DIR__)));
$dotenv->load();

return [
    'database' => [
        'driver' => 'mysql',
        'username' => getenv('MYSQL_USER') ?: '',
        'password' => getenv('MYSQL_PASSWORD') ?: '',
        'host' => getenv('MYSQL_HOST') ?: '',
        'database' => getenv('MYSQL_DATABASE') ?: '',
        'port' => getenv('MYSQL_PORT') ?: 3306,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
    ],
];

?>

